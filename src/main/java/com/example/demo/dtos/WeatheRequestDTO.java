package com.example.demo.dtos;

/**
 * To manage request related to weather
 * @author leopanch
 *
 */
public class WeatheRequestDTO {
	
	private int idApiWeather;
	private int idTemperature;
	private String cityCode;
	private String countryCode;
	
	public int getIdApiWeather() {
		return idApiWeather;
	}
	public void setIdApiWeather(int idApiWeather) {
		this.idApiWeather = idApiWeather;
	}
	public String getCityCode() {
		return cityCode;
	}
	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public int getIdTemperature() {
		return idTemperature;
	}
	public void setIdTemperature(int idTemperature) {
		this.idTemperature = idTemperature;
	}	
}
