package com.example.demo.dtos;

import com.example.demo.utils.Utils;

/**
 * To manage response weather
 * @author leopanch
 *
 */
public class WeatherResponseDTO extends Utils {

	private int idTemperature;
	private String temperatureName;
	private String cityName;
	private Double temp_min_celsius;
	private Double temp_max_celsius;
	private Double temp_min_fahrenheit;
	private Double temp_max_fahrenheit;
	private Double temp_min_kelvin;
	private Double temp_max_kelvin;

	public int getIdTemperature() {
		return idTemperature;
	}

	public void setIdTemperature(int idTemperature) {
		this.idTemperature = idTemperature;
	}

	public String getTemperatureName() {
		switch (this.idTemperature) {
		case 1:
			this.temperatureName = "Fahrenheit";
			break;
		case 2:
			this.temperatureName = "Celsius";
			break;
		case 3:
			this.temperatureName = "Kelvin";
			break;
		}
		return temperatureName;
	}

	public Double getTemp_min_fahrenheit() {
		return temp_min_fahrenheit;
	}

	public void setTemp_min_fahrenheit(Double temp_min_fahrenheit) {
		this.temp_min_fahrenheit = kelvinToFahrenheit(temp_min_fahrenheit);
	}

	public Double getTemp_max_fahrenheit() {
		return temp_max_fahrenheit;
	}

	public void setTemp_max_fahrenheit(Double temp_max_fahrenheit) {
		this.temp_max_fahrenheit = kelvinToFahrenheit(temp_max_fahrenheit);
	}

	public void setTemperatureName(String temperatureName) {
		this.temperatureName = temperatureName;
	}

	public Double getTemp_min_celsius() {
		return temp_min_celsius;
	}

	public void setTemp_min_celsius(Double temp_min_celsius) {
		this.temp_min_celsius = kelvinToCelsius(temp_min_celsius);
	}

	public Double getTemp_max_celsius() {
		return temp_max_celsius;
	}

	public void setTemp_max_celsius(Double temp_max_celsius) {
		this.temp_max_celsius = kelvinToCelsius(temp_max_celsius);
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public Double getTemp_min_kelvin() {
		return temp_min_kelvin;
	}

	public void setTemp_min_kelvin(Double temp_min_kelvin) {
		this.temp_min_kelvin = temp_min_kelvin;
	}

	public Double getTemp_max_kelvin() {
		return temp_max_kelvin;
	}

	public void setTemp_max_kelvin(Double temp_max_kelvin) {
		this.temp_max_kelvin = temp_max_kelvin;
	}
}
