package com.example.demo.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Weather {
	
	private Main main;

	public Main getMain() {
		return main;
	}
	public void setMain(Main main) {
		this.main = main;
	}		
}
