package com.example.demo.models;

import com.example.demo.utils.Utils;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Main extends Utils{
	private Double temp_min;
	private Double temp_max;
	
	public Double getTemp_min() {
		return temp_min;
	}
	public void setTemp_min(Double temp_min) {
		this.temp_min = temp_min;
	}
	public Double getTemp_max() {
		return temp_max;
	}
	public void setTemp_max(Double temp_max) {
		this.temp_max = temp_max;
	}

	public Double getTemp_min_celsius() {
		return kelvinToCelsius(this.temp_min);
	}
	public Double getTemp_max_celsius() {
		return kelvinToCelsius(this.temp_max);
	}
	
}
