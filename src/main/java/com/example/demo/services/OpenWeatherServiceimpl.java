package com.example.demo.services;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.example.demo.models.Weather;

@Service
public class OpenWeatherServiceimpl implements OpenWeatherAPIService {
	
	@Autowired
	RestTemplate restTemplate;
	
	private static final String OPEN_WEATHER_API_URL = "http://api.openweathermap.org/data/2.5/weather";
	private static final String APPID_NAME = "APPID";
	private static final String APPID_KEY_VALUE = "53798868960bc345424932399a9c5c38";
	private static final String COMMA = ",";

	@Override
	@Cacheable("openWeather")
	public Weather getCurrentWeatherCountryCity(String country, String city) {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		StringBuilder qValue = new StringBuilder();
		qValue.append(city).append(COMMA).append(country);
		
		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(OPEN_WEATHER_API_URL)
				.queryParam(APPID_NAME, APPID_KEY_VALUE)
				.queryParam("q", qValue);

		return restTemplate.getForObject(builder.toUriString(), Weather.class);
	}

}
