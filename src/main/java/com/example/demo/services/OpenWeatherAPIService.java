package com.example.demo.services;

import com.example.demo.models.Weather;

public interface OpenWeatherAPIService {

	/**
	 * This method gets the weather information through external API OpenWeatherMap
	 * 
	 * @param country
	 * @param city
	 * @return Weather
	 */
	Weather getCurrentWeatherCountryCity(String country, String city);
}
