package com.example.demo.utils;

public class Utils {

	/**
	 * Method transforms kelvin To Celsius
	 * @param number
	 * @return double
	 */
	protected static double kelvinToCelsius(Double number){
		return number - 273.15;	
	}
	
	/**
	 * Method transforms kelvin To Fahrenheit
	 * @param number
	 * @return double
	 */
	protected static double kelvinToFahrenheit(Double number){
		return (number * 1.8) - 459.67;	
	}
}
