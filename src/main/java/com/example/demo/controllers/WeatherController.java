package com.example.demo.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.validation.Valid;

import org.apache.catalina.mapper.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jackson.JsonObjectDeserializer;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import com.example.demo.dtos.WeatheRequestDTO;
import com.example.demo.dtos.WeatherResponseDTO;
import com.example.demo.models.Weather;
import com.example.demo.services.OpenWeatherAPIService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class WeatherController {

	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	OpenWeatherAPIService openWeatherAPIService;

	/**
	 * To get modelAndView 
	 * 
	 * @return ModelAndView de weather
	 */
	@GetMapping("/weather")
	public ModelAndView weatherForm() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("weatheRequesTemplate");
		mav.addObject("weatheRequest", new WeatheRequestDTO());
		return mav;
	}
	
	/**
	 * 
	 * Method POST in order to get client form information and return the information according the client choice
	 * 
	 * @param weatheRequestDTO
	 * @param bindingResult
	 * @return ModelAndView
	 */
	@PostMapping("/weather")
	public ModelAndView currentWeather(@ModelAttribute("weatheRequest") @Valid WeatheRequestDTO weatheRequestDTO, BindingResult bindingResult){

		ModelAndView mav = new ModelAndView();	
		Weather weather = openWeatherAPIService.getCurrentWeatherCountryCity(weatheRequestDTO.getCountryCode(), weatheRequestDTO.getCityCode());	
		
		WeatherResponseDTO weatherResponseDTO = new WeatherResponseDTO();
		weatherResponseDTO.setCityName(weatheRequestDTO.getCityCode());
		weatherResponseDTO.setIdTemperature(weatheRequestDTO.getIdTemperature());
		weatherResponseDTO.setTemp_max_celsius(weather.getMain().getTemp_min());
		weatherResponseDTO.setTemp_min_celsius(weather.getMain().getTemp_max());
		weatherResponseDTO.setTemp_max_fahrenheit(weather.getMain().getTemp_max());
		weatherResponseDTO.setTemp_min_fahrenheit(weather.getMain().getTemp_min());
		weatherResponseDTO.setTemp_max_kelvin(weather.getMain().getTemp_max());
		weatherResponseDTO.setTemp_min_kelvin(weather.getMain().getTemp_min());

		mav.setViewName("weatheResponseTemplate");
		mav.addObject("weather", weatherResponseDTO);
		return mav;	
	}
}
