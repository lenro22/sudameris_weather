package com.example.demo;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.models.Weather;
import com.example.demo.services.OpenWeatherAPIService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SudamerisProjectApplicationTests {
	
	@Autowired
    OpenWeatherAPIService openWeatherAPIService;

	@Test
	public void contextLoads() {
		Weather weather = openWeatherAPIService.getCurrentWeatherCountryCity("co", "Bogota");
		assertNotNull(weather);
	}

}
